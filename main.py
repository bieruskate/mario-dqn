import gym
import ppaquette_gym_super_mario

from wrappers import MarioActionSpaceWrapper

if __name__ == '__main__':
    env = gym.make('ppaquette/SuperMarioBros-1-1-v0')
    env = MarioActionSpaceWrapper(env)

    observation = env.reset()

    while True:
        action = env.action_space.sample()
        observation, reward, done, info = env.step(action)

        if done:
            observation = env.reset()
