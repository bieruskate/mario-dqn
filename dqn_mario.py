from __future__ import division

import argparse

import gym
import numpy as np
from PIL import Image
from keras.optimizers import Adam
from rl.agents.dqn import DQNAgent
from rl.callbacks import FileLogger, ModelIntervalCheckpoint
from rl.core import Processor
from rl.memory import SequentialMemory
from rl.policy import LinearAnnealedPolicy, EpsGreedyQPolicy

from model import get_model
from wrappers import MarioActionSpaceWrapper

import ppaquette_gym_super_mario

INPUT_SHAPE = (60, 120)
WINDOW_LENGTH = 4

X_CROP, Y_CROP = 1, 0.5


class MarioProcessor(Processor):
    def process_observation(self, observation):
        assert observation.ndim == 3
        img = Image.fromarray(observation)

        x, y = img.size
        img = img.crop((0, y * Y_CROP, x * X_CROP, y - 24))

        img = img.resize((INPUT_SHAPE[1], INPUT_SHAPE[0])).convert('L')
        processed_observation = np.array(img)

        assert processed_observation.shape == INPUT_SHAPE
        return processed_observation.astype('uint8')  # saves storage in experience memory

    def process_state_batch(self, batch):
        processed_batch = batch.astype('float32') / 255.
        return processed_batch

    def process_reward(self, reward):
        return np.clip(reward, -1., 1.)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--mode', choices=['train', 'test'], default='train')
    parser.add_argument('--env-name', type=str, default='ppaquette/SuperMarioBros-1-1-v0')
    parser.add_argument('--weights', type=str, default=None)
    return parser.parse_args()


args = parse_args()

env = gym.make('ppaquette/SuperMarioBros-1-1-v0')
env = MarioActionSpaceWrapper(env)
np.random.seed(123)
env.seed(123)
nb_actions = env.action_space.n

model = get_model(INPUT_SHAPE, WINDOW_LENGTH, nb_actions)
print(model.summary())

memory = SequentialMemory(limit=1000000, window_length=WINDOW_LENGTH)
processor = MarioProcessor()

policy = LinearAnnealedPolicy(EpsGreedyQPolicy(), attr='eps', value_max=1., value_min=.1, value_test=.05,
                              nb_steps=50000)

dqn = DQNAgent(model=model, nb_actions=nb_actions, policy=policy, memory=memory,
               processor=processor, nb_steps_warmup=2000, gamma=.99, target_model_update=1e-3,
               train_interval=4, delta_clip=1.)
dqn.compile(Adam(lr=1e-3), metrics=['mae'])

if args.mode == 'train':
    weights_filename = f'dqn_{args.env_name}_weights.h5f'
    checkpoint_weights_filename = 'dqn_' + args.env_name + '_weights_{step}.h5f'
    log_filename = f'dqn_{args.env_name}_log.json'
    callbacks = [ModelIntervalCheckpoint(checkpoint_weights_filename, interval=20000)]
    callbacks += [FileLogger(log_filename, interval=100)]
    dqn.fit(env, callbacks=callbacks, nb_steps=1750000, log_interval=20000)

    dqn.save_weights(weights_filename, overwrite=True)

    dqn.test(env, nb_episodes=10, visualize=False)
elif args.mode == 'test':
    weights_filename = f'dqn_{args.env_name}_weights.h5f'
    if args.weights:
        weights_filename = args.weights
    dqn.load_weights(weights_filename)
    dqn.test(env, nb_episodes=10, visualize=False)
